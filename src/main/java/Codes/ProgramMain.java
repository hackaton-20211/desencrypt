package Codes;

import java.io.*;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ProgramMain {

    static String numberO;
    static int[] number = new int[999];

    public static void main(String[] args) {
       ReadFile();
       Translate();
    }

    private static void ReadFile() {
        JSONParser jsonParser = new JSONParser();
        try {
            FileReader fr = new FileReader(System.getProperty("user.dir")+ "\\src\\main\\java\\JSON-Files\\Monlau_Data.json");
            Object ob = jsonParser.parse(fr);
            numberO = String.valueOf(ob);
            numberO = numberO.substring(1,numberO.length()-1);
            String[] res = numberO.split("[,]", 0);
            for(int i = 0; i< res.length; i++) {
                number[i] = Integer.parseInt(res[i]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private static void Translate(){

        int x = 0;
        int y = 0;
        int z = 0;
        int h = number.length;
        for (int counter = 0; counter < number.length; counter++)
        {
            if(number[counter] %2 == 0){
                y += number[counter];
            }else if(number[counter] %2 == 1){
                z += number[counter];
            }

            if (number[counter] > x)
            {
                x = number[counter];
            }
        }

        System.out.println("x " + x);
        System.out.println("y " + y);
        System.out.println("z " + z);
        System.out.println("h " + h);

        int result = 3*x-z+y-h;
        System.out.println("MONLAU-"+result);
    }
}
